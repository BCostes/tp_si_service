package ensg.eu.restws.data;

import java.util.List;
import java.util.Optional;

/**
 * Interface pour opération CRUD sur objet T
 * @author formation
 *
 * @param <T>
 */
public interface Dao<T> {
	/**
	 * Retourne l'objet d'identifiant id
	 * @param id
	 * @return
	 */
    Optional<T> one(long id);
    /**
     * Retourne tous les objets
     * @return
     */
    List<T> findAll();
    /**
     * Enregistre un nouvel objet
     * @param t
     */
    void save(T t);
    /**
     * Supprime l'objet d'identifiant id
     * @param id
     */
    void deleteById(long id);
    // TODO
    // update
}
