package ensg.eu.restws.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ensg.eu.restws.domain.Employee;

/**
 * Simule l'accès à une base de données / fichier de données
 * Implementer le pattern Singleton
 * @author formation
 *
 */
public class EmployeesDataset {
	
	private static EmployeesDataset instance = null;
	/**
	 * Liste des employés = table d'une base de données ...
	 */
	private List<Employee> employees;
	
	/**
	 * Constructeur privé
	 * La présence d'un constructeur privé supprime le constructeur public par défaut.
	 * Seul le singleton peut s'instancier lui-même.
	 */
	private EmployeesDataset() {
		super();
		// Quelques enregistrements à l'initialisation
		this.employees= new ArrayList<Employee>(
				Arrays.asList(new Employee("John"),new Employee("Sarah"),new Employee("Louise")));
	}
	
	/**
     * Méthode permettant de renvoyer l'unique instance de la classe EmployeesDataset
     * Pas de gestion du multi-threading (synchronized ...)
     * @return Retourne l'instance de EmployeesDataset
     */
    public final static EmployeesDataset getInstance() {
        if (EmployeesDataset.instance == null) {
        	EmployeesDataset.instance = new EmployeesDataset();
        }
        return EmployeesDataset.instance;
    }

	public List<Employee> getEmployees() {
		return employees;
	}

}
