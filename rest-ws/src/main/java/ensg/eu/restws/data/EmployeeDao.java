package ensg.eu.restws.data;

import java.util.List;
import java.util.Optional;

import ensg.eu.restws.domain.Employee;

/**
 * Opérations CRUD sur un objet Employee
 * Pas de base de données ici mais une liste d'employés manipulée en RAM
 * @author formation
 *
 */
public class EmployeeDao implements Dao<Employee> {
	
	/**
	 * Gestionnaire d'accès aux employés
	 */
	private EmployeesDataset employees;
			
	
	public EmployeeDao() {
		this.employees = EmployeesDataset.getInstance();
	}
	
	@Override
	public Optional<Employee> one(long id) {
		for(Employee e : this.employees.getEmployees()) {
			if (e.getId() == id) {
				return Optional.of(e);
			}
		}
		return Optional.empty();
	}

	@Override
	public void save(Employee t) {
		this.employees.getEmployees().add(t);
	}

	@Override
	public List<Employee> findAll() {
		return this.employees.getEmployees();
	}

	@Override
	public void deleteById(	long id) {
		Employee employee=null;
		for(Employee e : this.employees.getEmployees()) {
			if (e.getId() == id) {
				employee = e;
				break;
			}
		}
		if(employee != null) {
			this.employees.getEmployees().remove(employee);
		}
	}
}
