package ensg.eu.restws.domain;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Classe représentant l'objet métier employé
 * @author formation
 *
 */
public class Employee {
	
	
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if(!(o instanceof Employee)) {
			return false;
		}
		Employee e = (Employee)o;
		return e.getId() == this.getId();
	}
	
	public int hashCode() {
		return Objects.hash(this.id, this.name);
	}

}
