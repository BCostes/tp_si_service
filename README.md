# LICENCE


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.


# TP : développement d'un web-service RESTful avec Spring Boot

## Installation et configuration

### Installation java et maven

```shell
sudo apt install openjdk-11-jdk maven
```

### Installation Eclipse

Télécharger le logiciel depuis le site officiel : 

https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2023-06/R/eclipse-java-2023-06-R-linux-gtk-x86_64.tar.gz

Simplement extraire le contenu de l'archive dans Documents/Logiciels (créer le dossier si besoin)

### Importation du projet dans Eclipse

Lancer Eclipse.
File -> Import -> Maven -> Existing Maven Projects.
Sélectionner le répertoire TP2_SERVICES/rest-ws puis Finish.

Laisser le chargement du projet se terminer, ignorer les erreurs de compilation.

### Installation Postman

Télécharger le logiciel depuis le site officiel : 

https://www.postman.com/downloads/

Simplement extraire le contenu de l'archive dans Documents/Logiciels (créer le dossier si besoin)

## Préambule

Spring est un framework pour construire et définir l'infrastructure d'une application Java. Il s'agit d'un des framework les plus répandus. 
Spring se base beaucoup sur les *annotations* : https://en.wikipedia.org/wiki/Java_annotation.

Spring Boot est un framework Java facilitant le développement d'applications fondées sur Spring afin d'obtenir une application standalone (totalement autonome) packagée en jar.

Nous utiliserons Maven pour automatiser la mise en service de l'application. Maven est un outil d'automatisation de production des projets Java, qui s'occupe notamment de la gestion des dépendances, de la compilation, du packaging et du déploiement du projet.

## Objectif

L'objectif de cet exercice est de créer un web-service RESTful très simple avec Spring Boot en vous inspirant du tutorial : https://spring.io/guides/gs/rest-service/,  sans la partie configuration en commençant à la section *Create a Resource Representation Class*.

La ressource interrogée sera la classe *Employee* que vous allez écrire, représentant les employés d'une entreprise.


## Spécifications


Un employé est caractérisé par un identifiant (long) et un nom (String).

Votre web service REST devra prendre en charge les opérations et URL suivantes :

* **GET /employees** : retourne la liste des employés
* **GET /employees/{id}** : retourne l'employé dont l'identifiant vaut *id*
* **POST /employees?name=XX** : créé un nouvel employé
* **DELETE /employees/{id}** : supprime l'employé dont l'identifiant vaut *id*

Le diagramme de classe final de l'application est le suivant :


* Classe *Employee* : objet métier. Il s'agit de la ressource à laquelle on souhaite donner accès via notre service web
* Interface *DAO* : interface exposant les opérations CRUD, opérations de base pour la persistance des objets métier
* Classe *DAOEmployee* : implémentation de l'interface *DAO* pour gestion de la persistance des employés
* Classe *EmployeesDataset* : simule l'accès à une base de données ou un fichier de données des employés. Il s'agit d'une liste d'instances de la classe Employee. La classe *DAOEmployee* utilise l'unique instance (patron Singleton) pour gérer la persistance des employés, comme si ils étaient gérés en base. L'utilisation du pattern Singleton permet de s'assurer qu'une unique liste d'employées puisse être manipulée par l'application.
* Classe *EmployeeController*  : gère les requêtes HTTP d'accès à la ressource *Employee* identifiée par l'URI */employees* 
* Classe *RestWsApplication* : classe démarrant l'application Spring Boot

Le diagramme de classe est le suivant :

![Diagramme de classes](ressources/class_diag2.png)


A l'initialisation du projet, l'interface *DAO* et les classes *DAOEmployee*, *EmployeesDataset* et RestWsApplication sont déjà implémentées.

##  Développement


### Classe Employee


Implémentez la classe *Employee*.
Un squelette a déjà été créé. Complétez le sans toucher aux méthodes *equals* et *hashcode*.

### Classe EmployeeController

Implémentez la classe *EmployeeController* dans le package *controllers* en vous inspirant de l'exemple du tuto https://spring.io/guides/gs/rest-service/ (sans la partie configuration en commençant à la section *Create a Resource Representation Class*.).

Quelques indications :
* utilisez les annotations *@GetMapping*, *@PostMapping* et *@DeleteMapping* pour les méthodes définies dans le diagramme de classes
* n'oubliez pas l'annotation *@RestController* pour la classe
* pour la requête *POST*, utilisez l'annotation *@RequestParam*
* pour la requête *DELETE* et une des requête *GET*, utilisez l'annotation *PathVariable*.


## Lancer l'application 

* Compilez le projet maven comme suit depuis Eclipse(clean package) : 

![Compilation maven](ressources/mvn1.png)

ou : 
* clic droit sur le projet dans le package explorer puis *Run As -> Maven clean*
* puis clic droit sur le projet dans le package explorer puis *Run As -> Maven package*

ou encore depuis un terminal :
```
cd ~/Documents/TP2_SERVICES/rest-ws
mvn clean package
```


L'instruction *clean* permet d'effacer tous les fichiers générés par Maven (notamment le *.jar* exécutable du répertoire */home/formation/eclipse-workspace/rest-ws*).
L'instruction *package* compile le code source et génère un fichier *.jar* exécutable dans le répertoire *target*

## Tester le web-service

Lancez l'application autonome depuis Eclipse :

![Lancement de l'application standalone](ressources/mvn2.png)

ou depuis un terminal :

```
cd ~/Documents/TP2_SERVICES/rest-ws/target
java -jar rest-ws-0.0.1-SNAPSHOT.jar
```

Les requêtes *GET* peuvent être testée directement depuis un navigateur en visitant l'url *http://localhost:8080/employees* (ensemble des employés) ou *http://localhost:8080/employees/1* (employé d'identifiant 1) par exemple.

Les autres requêtes doivent être testées depuis un outils tiers, **Postman** ici (raccourci sur le bureau) :

![Postman](ressources/postman.png)

Fermer le terminal ou terminer l'exécution dans Eclipse avant de re-compiler le projet.
